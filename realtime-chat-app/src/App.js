// App.js
import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';

function App() {
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState('');
  const messagesEndRef = useRef(null);

  useEffect(() => {
    // Simuliere das Empfangen von Nachrichten in Echtzeit
    const interval = setInterval(() => {
      axios.get('/api/messages')
        .then(response => {
          setMessages(response.data);
          scrollToBottom();
        })
        .catch(error => console.error('Error fetching messages:', error));
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: 'smooth' });
  };

  const sendMessage = () => {
    axios.post('/api/messages', { text: newMessage })
      .then(() => setNewMessage(''))
      .catch(error => console.error('Error sending message:', error));
  };

  return (
    <div className="App">
      <div className="message-container">
        {messages.map((message, index) => (
          <div key={index} className="message">{message.text}</div>
        ))}
        <div ref={messagesEndRef}></div>
      </div>
      <div className="input-container">
        <input
          type="text"
          value={newMessage}
          onChange={e => setNewMessage(e.target.value)}
          placeholder="Type your message..."
        />
        <button onClick={sendMessage}>Send</button>
      </div>
    </div>
  );
}

export default App;
