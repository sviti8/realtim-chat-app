// server.js
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const PORT = 5000;

let messages = [];

app.use(bodyParser.json());

app.get('/api/messages', (req, res) => {
  res.json(messages);
});

app.post('/api/messages', (req, res) => {
  const { text } = req.body;
  messages.push({ text });
  res.status(201).send('Message sent successfully');
});

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});

